FROM php:8.2-cli

RUN pecl install redis
RUN docker-php-ext-enable redis

WORKDIR /app
