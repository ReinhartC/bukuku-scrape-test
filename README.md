## Prerequisites
- Composer - Latest
- Docker (Docker Compose) - Latest

## Setup
1) composer install (or update)
2) docker-compose up
3) php artisan serve
