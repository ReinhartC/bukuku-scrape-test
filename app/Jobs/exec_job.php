<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use App\Services\RedisService;

$redis = new Redis();
$redis->connect('redis', 6379);

$existing = $redis->lrange('jobs', 0, -1);

if (!$existing) echo "Tidak ada Cleanup Job.";
else {
    $jobData = json_decode($redis->brpop('jobs', 0)[1], true);
    $jobType = $jobData['type'];

    $job = match ($jobType) {
            'cleanup' => new RedisService(),
        default => throw new \Exception("Job [{$jobType}] tidak ditemukan!")
    };

    $job->execute('delete');
}

echo PHP_EOL;
