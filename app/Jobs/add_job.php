<?php

$redis = new \Redis();
$redis->connect('redis', 6379);

$existing = $redis->lrange('jobs', 0, -1);

if ($existing) echo "Cleanup Job sudah ada.";
else {
    $redis->lpush('jobs', json_encode(['type' => 'cleanup']));
    echo "Cleanup Job berhasil ditambahkan!";
}

echo PHP_EOL;
