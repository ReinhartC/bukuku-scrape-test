<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedisController extends Controller
{
    public function call($command){
        if(in_array($command, array('add_job', 'exec_job'))) {
            # Adjust REDIS_CONTAINER in env
            $shell = "docker exec ". env('REDIS_CONTAINER') ." /bin/bash -c 'php app/Jobs/". $command .".php'";

            $status = exec($shell);
        }
        else $status = "Perintah tidak ditemukan!";

        return back()->with('status', $status);
    }
}
