<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Services\ScrapeService;
use View;

class MainController extends Controller
{
    // Public
    public function index(){
        $json = $this->get_data();
        $this->generate_file($json);

        return View::make('main')->with('json', $json);
    }

    // Private
    private function get_data(){
        $data = (new ScrapeService)->get_page('http://kursdollar.org');
        return json_encode($data, JSON_PRETTY_PRINT);
    }

    private function generate_file($data){
        $filename = "rate-" . date("d-m-Y--H-i-s", strtotime('+7 hours'));
        Storage::disk('public')->put($filename, $data);
    }
}
