<?php

namespace App\Services;

Class ScrapeService
{
    // Public
    public function get_page($url){
        $client = new \Goutte\Client();
        $response = $client->request('GET', $url);
        return $this->scrape_page($response);
    }

    // Private
    private function scrape_page($response){
        // Meta
        $meta_i = $response->evaluate('//div[@class="row space30"]//div[@class="col-md-12"]//table[@class="in_table"]//tr[@class="title_table"]//td[@colspan="3"]');
        $meta_w = $response->evaluate('//div[@class="row space30"]//div[@class="col-md-12"]//table[@class="in_table"]//tr[@class="title_table"]//td[@rowspan="2"][@colspan="2"]');

        $meta = $this->generate_meta($meta_i, $meta_w);

        // Rates
        $currencies = $response->evaluate('//div[@class="row space30"]//div[@class="col-md-12"]//table[@class="in_table"]//tr//td[@align="center"]//a');
        $prices = $response->evaluate('//div[@class="row space30"]//div[@class="col-md-12"]//table[@class="in_table"]//tr//td');

        $rates = $this->generate_rate($currencies, $prices);

        return array('meta'=>$meta, 'rates'=>$rates);
    }

    private function generate_meta($meta_i, $meta_w){
        foreach ($meta_i as $key => $meta) $indonesia = $meta->textContent;
        foreach ($meta_w as $key => $meta) $world = $meta->textContent;

        $meta = array(
            'date'=> date("d-m-Y", strtotime('+7 hours')),
            'day'=> date("l", strtotime('+7 hours')),
            'indonesia'=> $indonesia,
            'world'=>substr($world, 0, 10)." - ".substr($world, 10)
        );

        return $meta;
    }

    private function generate_rate($currencies, $prices){
        $rates = array();
        $prices = $this->parse_prices($prices);

        $price_index = 0;
        foreach ($currencies as $key => $currency) {
            if(strlen($currency->textContent)>3) continue;

            $rate = array(
                'currency'=>$currency->textContent,
                'buy'=>$prices[$price_index],
                'sell'=>$prices[$price_index+1],
                'average'=>$prices[$price_index+2],
                'world_rate'=>$prices[$price_index+3]
            );
            array_push($rates, $rate);

            $price_index += 4;
        }

        return $rates;
    }

    private function parse_prices($prices){
      $parsed_price = array();

      foreach ($prices as $key => $price)
        if((int)$price->textContent > 0) {
            $price = preg_replace("/\s+|\([^)]+\)/", "", $price->textContent);
            array_push($parsed_price, $price);
          };

      return $parsed_price;
    }
}
